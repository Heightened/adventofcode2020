#include <iostream>
#include <regex>
#include <map>

#include "file.h"
#include "utilitystring.h"
#include "stringset.h"

class ValidRange {
    int minimum;
    int maximum;

public:
    ValidRange(String rangeString) {
        rangeString = rangeString.strip();
        auto tokens = rangeString.split('-');

        if (tokens.size() != 2) {
            throw std::runtime_error("Syntax error, invalid range format");
        }

        minimum = stoi(tokens[0]);
        maximum = stoi(tokens[1]);
    }

    bool validate(int value) {
        return minimum <= value && value <= maximum;
    }
};

class Rule {
    std::string fieldName;
    std::vector<ValidRange> ranges;

public:
    Rule(std::string fieldName, String ruleInput) : fieldName(fieldName) {
        auto tokens = ruleInput.split("or");

        if (tokens.size() != 2) {
            throw std::runtime_error("Syntax error, invalid rule format");
        }

        for (String& validRangeString : tokens) {
            ranges.push_back(ValidRange(validRangeString));
        }
    }

    bool validate(int value) {
        for (ValidRange& range : ranges) {
            if (range.validate(value)) {
                return true;
            }
        }
        return false;
    }

    std::string getFieldName() {
        return fieldName;
    }
};

class TicketRules {
    std::vector<Rule> rules;

public:
    void addRule(String line) {
        auto tokens = line.split(':');

        if (tokens.size() != 2) {
            throw std::runtime_error("Syntax error, invalid ticket rule format");
        }

        rules.push_back(Rule(tokens[0], tokens[1]));
    }

    bool validateForAny(int value) {
        for (Rule& rule : rules) {
            if (rule.validate(value)) {
                return true;
            }
        }
        return false;
    }

    StringSet getAllFieldNames() {
        StringSet fieldNames;
        for (Rule& rule : rules) {
            fieldNames.insert(rule.getFieldName());
        }
        return fieldNames;
    }

    StringSet validForWhich(int value) {
        StringSet validFields;
        for (Rule& rule : rules) {
            if (rule.validate(value)) {
                validFields.insert(rule.getFieldName());
            }
        }
        return validFields;
    }
};

class Ticket {
    std::vector<int> ticketValues;

public:
    Ticket(String ticketInput) {
        auto valueStrings = ticketInput.split(',');
        for (std::string& value : valueStrings) {
            ticketValues.push_back(stoi(value));
        }
    }

    std::vector<int> getErrorValues(TicketRules& rules) {
        std::vector<int> errorValues;

        for (int& value : ticketValues) {
            if (!rules.validateForAny(value)) {
                errorValues.push_back(value);
            }
        }

        return errorValues;
    }

    // Allow casting to vector of values
    operator std::vector<int>() const {
        return ticketValues;
    }

    // Allow reading an element of the ticketValues using the [] operator
    int operator[](int index) const {
        return ticketValues[index];
    }
};

void parseTicketFile(File& file, TicketRules& ticketRules, Ticket*& myTicket, std::vector<Ticket>& tickets) {
    auto lines = file.getLines();
    int i = 0;

    // Parse first section of input
    for (i; i < lines.size(); i++) {
        if (lines[i] == "") {
            // Skip lines
            i += 2;
            // Move onto next stage
            break;
        }

        ticketRules.addRule(lines[i]);
    }

    // Parse your ticket
    for (i; i < lines.size(); i++) {
        if (lines[i] == "") {
            // Skip lines
            i += 2;
            // Move onto next stage
            break;
        }

        myTicket = new Ticket(lines[i]);
    }

    // Parse nearby tickets
    for (i; i < lines.size(); i++) {
        tickets.push_back(Ticket(lines[i]));
    }
}

int solveDay16Part1(File& file) {
    TicketRules ticketRules;
    Ticket* myTicket = nullptr;
    std::vector<Ticket> tickets;

    parseTicketFile(file, ticketRules, myTicket, tickets);

    // Calculate ticket scanning error rate
    int errorRate = 0;
    for (Ticket& ticket : tickets) {
        std::vector<int> errorValues = ticket.getErrorValues(ticketRules);
        for (int& value : errorValues) {
            errorRate += value;
        }
    }

    return errorRate;
}

uint64_t solveDay16Part2(File& file) {
    TicketRules ticketRules;
    Ticket* myTicket = nullptr;
    std::vector<Ticket> tickets;

    parseTicketFile(file, ticketRules, myTicket, tickets);

    // Filter valid tickets
    std::vector<Ticket> validTickets;
    for (Ticket& ticket : tickets) {
        if (ticket.getErrorValues(ticketRules).size() == 0) {
            validTickets.push_back(ticket);
        }
    }

    // Fields to be identified
    StringSet toBeIdentified = ticketRules.getAllFieldNames();
    StringSet identified; //!< Empty to start

    // Get values on your ticket
    std::vector<int> myTicketValues = *myTicket;

    // Named values will be stored here
    std::map<std::string, int> namedTicketValues;

    while (toBeIdentified.size() > 0) {
        // Check value by value what they could be
        for (int i = 0; i < myTicketValues.size(); i++) {
            // Candidates
            StringSet fieldCandidates = toBeIdentified;
            // Field needs to be valid for every valid ticket
            for (Ticket& ticket : validTickets) {
                StringSet validFields = ticketRules.validForWhich(ticket[i]);
                fieldCandidates.intersectSet(validFields);
            }

            if (fieldCandidates.size() == 1) {
                std::string fieldName = *fieldCandidates.begin();
                identified.insert(fieldName);
                toBeIdentified.erase(fieldName);
                std::cout << "Field i=" << i << " represents " << fieldName << ", " << myTicketValues[i] << " on your ticket" << std::endl;
                namedTicketValues[fieldName] = myTicketValues[i];
            }
        }
    }

    // Find all that start with departure and multiply the values
    uint64_t product = 1;
    std::vector<int> valuesToMultiply;
    for (auto pair : namedTicketValues) {
        if (pair.first.find("departure") == 0) {
            product *= pair.second;
            valuesToMultiply.push_back(pair.second);
        }
    }
    return product;
}

int main(int argc, char** args) {
    std::cout << "Day 16" << std::endl;

    File exampleFile("../../day16/example.txt");
    File inputFile("../../day16/input.txt");
    File exampleFile2("../../day16/example_part2.txt");

    std::cout << "Result Part 1 example: " << solveDay16Part1(exampleFile) << " (should equal 71)" << std::endl;
    std::cout << "Result Part 1: " << solveDay16Part1(inputFile) << std::endl;
    std::cout << "Result Part 2 example: " << solveDay16Part2(exampleFile2) << " (class is 12, row is 11, and seat is 13)" << std::endl;
    std::cout << "Result Part 2: " << solveDay16Part2(inputFile) << std::endl;
}
