#include <iostream>
#include <map>
#include <regex>

#include "file.h"
#include "utilitystring.h"

class SeatingList {
    std::vector<std::pair<int, int>> seatList;

public:
    SeatingList(File& inputFile) {
        auto lines = inputFile.getLines();

        for (std::string line : lines) {
            String rowString = line.substr(0, 7);
            String columnString = line.substr(7, 3);

            // The row number is indicated in binary using F / B for 0 / 1
            rowString.replaceCharacter('F', '0');
            rowString.replaceCharacter('B', '1');
            int row = std::stoi(rowString, nullptr, 2);

            // The column number is indicated in binary using L / R for 0 / 1
            columnString.replaceCharacter('L', '0');
            columnString.replaceCharacter('R', '1');
            int column = std::stoi(columnString, nullptr, 2);

            // Push back the row and column
            seatList.push_back({ row, column });
        }

    }

    int getHighestId() {
        int max = 0;

        for (auto seat : seatList) {
            int seatId = seat.first * 8 + seat.second;

            if (seatId > max) {
                max = seatId;
            }
        }

        return max;
    }

    bool seatInList(int seatId) {
        for (auto seat : seatList) {
            int seatIdInList = seat.first * 8 + seat.second;
            if (seatId == seatIdInList) {
                return true;
            }
        }
        // Not found
        return false;
    }

    int findMySeat() {
        for (auto seatToMyLeft : seatList) {
            int seatIdA = seatToMyLeft.first * 8 + seatToMyLeft.second;
            for (auto seatToMyRight : seatList) {
                int seatIdB = seatToMyRight.first * 8 + seatToMyRight.second;

                if (seatIdA + 2 == seatIdB) {
                    // Candidate seat, is this one in the list already?
                    if (!seatInList(seatIdA + 1)) {
                        return seatIdA + 1;
                    }

                }
            }
        }
        // Not found
        return -1;
    }
};

int solveDay5Part1(SeatingList& seats) {
    return seats.getHighestId();
}

int solveDay5Part2(SeatingList& seats) {
    return seats.findMySeat();
}

int main(int argc, char** args) {
    std::cout << "Day 5" << std::endl;

    File exampleFile("../../day5/example.txt");
    SeatingList exampleSeats(exampleFile);

    File inputFile("../../day5/input.txt");
    SeatingList inputSeats(inputFile);

    std::cout << "Result Part 1 example: " << solveDay5Part1(exampleSeats) << " (should equal 820)" << std::endl;
    std::cout << "Result Part 1: " << solveDay5Part1(inputSeats) << std::endl;
    std::cout << "Result Part 2: " << solveDay5Part2(inputSeats) << std::endl;
}
