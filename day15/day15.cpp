#include <iostream>
#include <map>

#include "file.h"
#include "utilitystring.h"

class Series {
    uint64_t lastNumber;
    size_t seriesSize;

    std::map<int, size_t> beforeLastOccurences;
    std::map<int, size_t> lastOccurences;

    void pushBack(uint64_t number) {
        lastNumber = number;
        seriesSize++;

        if (lastOccurences.count(number) > 0) {
            beforeLastOccurences[number] = lastOccurences[number];
        }
        lastOccurences[number] = seriesSize;
    }

public:
    Series(String initialization) {
        seriesSize = 0;

        auto seriesStrings = initialization.split(',');

        for (auto numberString : seriesStrings) {
            int number = stoi(numberString);
            pushBack(number);
        }
    }

    int get(size_t index) {
        while (index > seriesSize) {
            int last = lastNumber;

            if (beforeLastOccurences.count(last) < 1) {
                pushBack(0);
            }
            else {
                int difference = seriesSize - beforeLastOccurences[last];
                pushBack(difference);
            }
        }

        return lastNumber;
    }
};

int solveDay15Part1(File& file) {
    Series series(file.getLines()[0]);

    return series.get(2020);
}

int solveDay15Part2(File& file) {
    Series series(file.getLines()[0]);

    return series.get(30000000);
}

int main(int argc, char** args) {
    std::cout << "Day 15" << std::endl;

    File exampleFile("../../day15/example.txt");
    File inputFile("../../day15/input.txt");

    std::cout << "Result Part 1 example: " << solveDay15Part1(exampleFile) << " (should equal 436)" << std::endl;
    std::cout << "Result Part 1: " << solveDay15Part1(inputFile) << std::endl;
    std::cout << "Result Part 2 example a: " << solveDay15Part2(exampleFile) << " (should equal 175594)" << std::endl;
    std::cout << "Result Part 2: " << solveDay15Part2(inputFile) << std::endl;
}
