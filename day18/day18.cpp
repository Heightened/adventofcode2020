#include <iostream>

#include "file.h"
#include "utilitystring.h"

class SubExpression {
    uint64_t constant;
    char operation;
    SubExpression* left;
    SubExpression* right;

public:
    SubExpression(int constant) :
        constant(constant), operation(' ')
    {
    }

    SubExpression(SubExpression* left, char operation, SubExpression* right) :
        left(left), operation(operation), right(right)
    {
    }

    uint64_t evaluate() {
        if (operation == '+') {
            return left->evaluate() + right->evaluate();
        }
        else if (operation == '*') {
            return left->evaluate() * right->evaluate();
        }
        else {
            return constant;
        }
    }
};

// Return term immediately left of the index character
std::string getLeftTerm(std::string& expression, size_t index) {
    String term = "";
    int bracketLevel = 0;
    for (int c = index - 1; c >= 0; c--) {
        if (expression[c] == ')') {
            bracketLevel++;
        }
        else if (expression[c] == '(') {
            bracketLevel--;
        }

        // Extend the term string
        term = expression[c] + term;

        // Don't stop before we have something meaningful in the term string
        if (term.strip() != "") {
            // Matching bracket found (or no brackets at all)
            if (bracketLevel == 0) {
                // This would not be enough if numbers in the input consisted of more than one digit
                break;
            }
        }
    }
    return term;
}

// Return term immediately right of the index character
std::string getRightTerm(std::string& expression, size_t index) {
    String term = "";
    int bracketLevel = 0;
    for (int c = index + 1; c < expression.size(); c++) {
        if (expression[c] == '(') {
            bracketLevel++;
        }
        else if (expression[c] == ')') {
            bracketLevel--;
        }

        // Extend the term string
        term.push_back(expression[c]);

        // Don't stop before we have something meaningful in the term string
        if (term.strip() != "") {
            // Matching bracket found (or no brackets at all)
            if (bracketLevel == 0) {
                // This would not be enough if numbers in the input consisted of more than one digit
                break;
            }
        }
    }
    return term;
}

class MathExpression {
    SubExpression* expression;

public:
    SubExpression* getExpression() {
        return expression;
    }

    MathExpression(String expressionString) : expression(nullptr) {
        // Get rid of excess spaces
        expressionString = expressionString.strip();

        // Construct tree according to precedence rules
        auto tokens = expressionString.split();

        if (tokens.size() == 0) {
            throw std::runtime_error("Something went wrong in parsing the math expression");
        }

        if (expressionString[expressionString.size() - 1] == ')') {
            // Right side will contain everything in the brackets
            // Find matching bracket
            String rightSide = getLeftTerm(expressionString, expressionString.size());

            if (expressionString.size() > rightSide.size()) {
                // Left side will contain the remainder, left of the right sub-expression
                String remainder = expressionString.substr(0, expressionString.size() - rightSide.size());
                auto remainingTokens = remainder.split();

                // Remove outer brackets
                rightSide = rightSide.substr(1, rightSide.size() - 2);
                // Evaluate expression strings further
                MathExpression* leftExpression = new MathExpression(remainder.substr(0, remainder.size() - 3));
                char operation = remainingTokens[remainingTokens.size() - 1][0];
                MathExpression* rightExpression = new MathExpression(rightSide);
                // Last remaining token is expected to be operator
                expression = new SubExpression(leftExpression->getExpression(), operation, rightExpression->getExpression());
            }
            else {
                // Remove outer brackets
                rightSide = rightSide.substr(1, rightSide.size() - 2);
                // Evaluate expression strings further
                MathExpression* rightExpression = new MathExpression(rightSide);
                expression = rightExpression->getExpression();
            }
        }
        else if (tokens.size() == 1) {
            // Constant
            expression = new SubExpression(stoi(tokens[0]));
        }
        else {
            // Assume the last token is a number
            size_t lastTokenIndex = tokens.size() - 1;
            SubExpression* right = new SubExpression(stoi(tokens[lastTokenIndex]));
            // Right side requires further evaluation
            size_t rightSize = 1 + tokens[lastTokenIndex - 1].size() + 1 + tokens[lastTokenIndex].size();
            MathExpression* left = new MathExpression(expressionString.substr(0, expressionString.size() - rightSize));
            // Assume the second to last token is an operator
            expression = new SubExpression(left->getExpression(), tokens[lastTokenIndex - 1][0], right);
        }
    }
};

uint64_t solveDay18Part1(File& file) {
    auto lines = file.getLines();

    uint64_t sum = 0;
    for (std::string line : lines) {
        MathExpression expression(line);
        sum += expression.getExpression()->evaluate();
    }

    return sum;
}

void prioritize(std::string& string, size_t operatorIndex) {
    std::string leftTerm = getLeftTerm(string, operatorIndex);
    std::string rightTerm = getRightTerm(string, operatorIndex);

    std::string leftRemainder = string.substr(0, operatorIndex - leftTerm.size());
    std::string rightRemainder = string.substr(operatorIndex + rightTerm.size() + 1, string.size());

    std::string newString = leftRemainder + '(' + leftTerm + ' ' + string[operatorIndex] + ' ' + rightTerm + ')' + rightRemainder;
    string = newString;
}

uint64_t solveDay18Part2(File& file) {
    auto lines = file.getLines();

    uint64_t sum = 0;
    for (std::string line : lines) {
        for (int i = 0; i < line.size(); i++) {
            if (line[i] == '+') {
                prioritize(line, i);
                // Avoid evaluating the same operator again
                i += 2;
            }
        }

        MathExpression expression(line);

        uint64_t subTotal = expression.getExpression()->evaluate();
        sum += subTotal;
    }

    return sum;
}

int main(int argc, char** args) {
    std::cout << "Day 18" << std::endl;

    File exampleFile("../../day18/example.txt");
    File inputFile("../../day18/input.txt");

    std::cout << "Result Part 1 example: " << solveDay18Part1(exampleFile) << " (should equal 26457 = 71 + 51 + 26 + 437 + 12240 + 13632)" << std::endl;
    std::cout << "Result Part 1: " << solveDay18Part1(inputFile) << std::endl;
    std::cout << "Result Part 2 example: " << solveDay18Part2(exampleFile) << " (should equal 694173 = 231 + 51 + 46 + 1445 + 669060 + 23340)" << std::endl;
    std::cout << "Result Part 2: " << solveDay18Part2(inputFile) << std::endl;
}
