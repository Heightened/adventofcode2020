#include <iostream>

#include "file.h"
#include "utilitystring.h"

// Each line gives the password policy and then the password.
// The password policy indicates the lowestand highest number of times a given letter must appear for the password to be valid.
// For example, 1 - 3 a means that the password must contain a at least 1 time and at most 3 times.
class PasswordEntry {
    unsigned int minimum;
    unsigned int maximum;
    char character;
    std::string password;

    size_t characterOccurences;

public:
    PasswordEntry(std::string inputLine) {
        String line(inputLine);

        auto tokens = line.split();

        if (tokens.size() < 3) {
            throw std::runtime_error("Line incomplete, less than three tokens");
        }

        // Get minimum and maximum occurences
        auto limits = tokens[0].split('-');
        if (limits.size() < 2) {
            throw std::runtime_error("Line format wrong, no limits defined");
        }
        minimum = std::stoi(limits[0]);
        maximum = std::stoi(limits[1]);

        // Get first character of second token
        character = tokens[1][0];

        // Store third token as password string
        password = tokens[2];

        // Count character occurences
        characterOccurences = tokens[2].count(character);
    }

    bool policy1RequirementsMet() {
        bool met = minimum <= characterOccurences && characterOccurences <= maximum;
        std::cout << (met ? "TRUE " : "FALSE") << " : " << minimum << " <= " << characterOccurences << " <= " << maximum << std::endl;
        return met;
    }

    bool policy2RequirementsMet() {
        bool met = password[minimum - 1] == character ^ password[maximum - 1] == character;
        std::cout << (met ? "TRUE " : "FALSE") << " : " << password[minimum - 1] << " == " << character << " XOR " << character << " == " << password[maximum - 1] << std::endl;
        return met;
    }
};

int solveDay2Part1(File& inputFile) {
    auto lines = inputFile.getLines();

    // Check password requirements, line by line
    size_t count = 0;
    for (auto line : lines) {
        PasswordEntry entry(line);
        if (entry.policy1RequirementsMet()) {
            count++;
        }
    }
    return count;
}

int solveDay2Part2(File& inputFile) {
    auto lines = inputFile.getLines();

    // Check password requirements, line by line
    size_t count = 0;
    for (auto line : lines) {
        PasswordEntry entry(line);
        if (entry.policy2RequirementsMet()) {
            count++;
        }
    }
    return count;
}

int main(int argc, char** args) {
    std::cout << "Day 2" << std::endl;

    File inputFile("../../day2/input.txt");

    std::cout << "Result Part 1: " << solveDay2Part1(inputFile) << std::endl;
    std::cout << "Result Part 2: " << solveDay2Part2(inputFile) << std::endl;
}
