#include <iostream>
#include <set>

#include "file.h"
#include "utilitystring.h"

/*!
 * Struct to represent the state of the program
 * Affected by all operations
 */
struct State {
    int programCounter;
    int accumulator;
};

/*!
 * Abstract class to represent instructions
 * Executing each instruction can affect the state in some way
 */
class Instruction {
public:
    virtual void execute(State& state) = 0;
};

/*!
 * Accumulator instruction to modify the accumulator value by a certain amount
 */
class AccInstruction : public Instruction {
    int valueStep;

public:
    AccInstruction(int valueStep) : valueStep(valueStep) {
    }

    // Inherited via Instruction
    virtual void execute(State& state) override {
        state.accumulator += valueStep;
        // Increase program counter after instruction
        state.programCounter++;
    }
};

/*!
 * Jump instruction to jump to another part of the code
 */
class JmpInstruction : public Instruction {
    int jumpStep;

public:
    JmpInstruction(int jumpStep) : jumpStep(jumpStep) {
    }

    // Inherited via Instruction
    virtual void execute(State& state) override {
        state.programCounter += jumpStep;
    }
};

/*!
 * Nop instruction represent any other operation which only advances the program
 */
class NopInstruction : public Instruction {
    int value;

public:
    NopInstruction(int valueStep) : value(value) {
    }

    // Inherited via Instruction
    virtual void execute(State& state) override {
        // No operation
        // Only increase program counter
        state.programCounter++;
    }
};

/*!
 * Class to respresent the code as a list of instructions
 */
class BootCode {
    std::vector<Instruction*> instructions;

public:
    BootCode(std::vector<std::string>& lines) {
        for (String line : lines) {
            auto tokens = line.split();

            if (tokens.size() != 2) {
                throw std::runtime_error("Syntax error, wrong number of arguments");
            }

            // Parse parameter
            int parameter = std::stoi(tokens[1]);

            // Parse command
            if (tokens[0] == "acc") {
                instructions.push_back(new AccInstruction(parameter));
            }
            else if (tokens[0] == "jmp") {
                instructions.push_back(new JmpInstruction(parameter));
            }
            else if (tokens[0] == "nop") {
                instructions.push_back(new NopInstruction(parameter));
            }
            else {
                throw std::runtime_error("Syntax error, unkown keyword");
            }
        }
    }

    BootCode(File& file) : BootCode(file.getLines()) {
    }

    Instruction* getInstruction(size_t programCounter) {
        return instructions[programCounter];
    }

    size_t size() {
        return instructions.size();
    }
};

/*!
 * Class to execute BootCode, step by step
 */
class Emulator {
    State state;
    BootCode& code;

public:
    Emulator(BootCode& code) :
        state({0, 0}),
        code(code)
    {
    }

    /*!
     * Function to step through the code
     */
    void step() {
        Instruction* instruction = code.getInstruction(state.programCounter);
        instruction->execute(state);
    }

    /*!
     * Returns the current value of the accumulator
     */
    int getAccumulator() {
        return state.accumulator;
    }

    /*!
     * Returns the current value of the program counter
     */
    int getProgramCounter() {
        return state.programCounter;
    }

    /*!
     * Returns whether the program has terminated
     */
    bool terminated() {
        return state.programCounter == code.size();
    }
};

bool terminates(BootCode& code, State& endState) {
    std::set<int> colored;

    Emulator emulator(code);

    while (!colored.count(emulator.getProgramCounter())) {
        // Color program lines that have been covered
        colored.insert(emulator.getProgramCounter());
        // Execute the current line
        emulator.step();
        // Check whether this has terminated the program
        if (emulator.terminated()) {
            endState = { emulator.getProgramCounter(), emulator.getAccumulator() };
            return true;
        }
    }

    // Loop detected
    endState = { emulator.getProgramCounter(), emulator.getAccumulator() };
    return false;
}

bool terminates(BootCode& code) {
    // We don't care about the end state in this function
    State state;
    return terminates(code, state);
}

int solveDay8Part1(BootCode& code) {
    State endState;
    terminates(code, endState);
    return endState.accumulator;
}

int solveDay8Part2(File& file) {
    int offset = 0;
    size_t fileSize = file.getLines().size();
    while (offset < fileSize) {
        auto lines = file.getLines();
        for (int i = offset; i < lines.size(); i++) {
            auto tokens = String(lines[i]).split();
            if (tokens[0] == "jmp") {
                lines[i] = "nop " + tokens[1];
                offset = i + 1;
                break;
            }
            else if (tokens[0] == "nop") {
                lines[i] = "jmp " + tokens[1];
                offset = i + 1;
                break;
            }
        }

        BootCode modifiedCode(lines);
        State endState;
        if (terminates(modifiedCode, endState)) {
            return endState.accumulator;
        }
    }
}

int main(int argc, char** args) {
    std::cout << "Day 8" << std::endl;

    File exampleFile("../../day8/example.txt");
    BootCode exampleCode(exampleFile);

    File inputFile("../../day8/input.txt");
    BootCode inputCode(inputFile);

    File exampleModifiedFile("../../day8/example_modified.txt");
    BootCode exampleModifiedCode(exampleModifiedFile);

    std::cout << "Result Part 1 example: " << solveDay8Part1(exampleCode) << " (should equal 5)" << std::endl;
    std::cout << "Result Part 1: " << solveDay8Part1(inputCode) << std::endl;

    std::cout << "Result Part 2 example terminates: " << (terminates(exampleCode) ? "true" : "false") << " (should equal false)" << std::endl;
    std::cout << "Result Part 2 example modified terminates: " << (terminates(exampleModifiedCode) ? "true" : "false") << " (should equal true)" << std::endl;
    std::cout << "Result Part 2 input terminates: " << (terminates(inputCode) ? "true" : "false") << " (should equal false)" << std::endl;

    std::cout << "Result Part 2 example: " << solveDay8Part2(exampleFile) << " (should equal 8)" << std::endl;
    std::cout << "Result Part 2: " << solveDay8Part2(inputFile) << std::endl;
}
