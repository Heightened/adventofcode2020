cmake_minimum_required (VERSION 2.6)
project (Day8)

add_executable (Day8 day8.cpp)
target_link_libraries(Day8 Utility)
