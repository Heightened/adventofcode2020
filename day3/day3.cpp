#include <iostream>

#include "file.h"
#include "utilitystring.h"

/*!
 * Due to the local geology, trees in this area only grow on exact integer coordinates in a grid.
 * You make a map(your puzzle input) of the open squares(.) and trees(#) you can see.
 *
 * ..##.......
 * #...#...#..
 *
 * These aren't the only trees, though;
 * due to something you read about once involving arboreal genetics and biome stability, the same pattern repeats to the right many times:
 *
 * ..##.........##.........##.........##.........##.........##.......
 * #...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
 */
class RecurringMap {
    bool** map;

    size_t height;
    size_t width;

public:
    RecurringMap(File& file, char trueCharacter) {
        auto lines = file.getLines();
        auto firstLine = lines[0];

        // Establish map size
        height = lines.size();
        width = firstLine.size();

        // Make map matrix
        map = new bool* [height];
        for (int i = 0; i < height; i++) {
            map[i] = new bool[width];
        }

        // Fill in matrix
        int x = 0;
        int y = 0;
        for (std::string& line : lines) {
            for (char& c : line) {
                map[y][x] = c == trueCharacter;
                // Move cursor to the right
                x++;
            }
            // Move cursor down
            y++;
            // Move cursor back to the left-most position
            x = 0;
        }
    }

    bool isTrue(unsigned int x, unsigned int y) {
        if (y >= height) {
            throw std::runtime_error("Specified y is below map");
        }

        return map[y][x % width];
    }

    size_t getHeight() {
        return height;
    }
};

int getTreesEncounteredByTraversalMethod(RecurringMap& map, size_t stepRight, size_t stepDown) {
    int treesEncountered = 0;

    // Traverse the map
    int x = 0;
    for (int y = 0; y < map.getHeight(); y += stepDown) { // Move down
        if (map.isTrue(x, y)) {
            treesEncountered++;
        }
        x += stepRight; // Move right
    }

    return treesEncountered;
}

// You start on the open square(.) in the top - left corner and need to reach the bottom(below the bottom - most row on your map).
// The toboggan can only follow a few specific slopes(you opted for a cheaper model that prefers rational numbers);
// start by counting all the trees you would encounter for the slope right 3, down 1:
// From your starting position at the top - left, check the position that is right 3 and down 1.
// Then, check the position that is right 3 and down 1 from there, and so on until you go past the bottom of the map.
int solveDay3Part1(RecurringMap& map) {
    return getTreesEncounteredByTraversalMethod(map, 3, 1);
}

// Determine the number of trees you would encounter if, for each of the following slopes, you start at the top - left corner and traverse the map all the way to the bottom :
// Right 1, down 1.
// Right 3, down 1. (This is the slope you already checked.)
// Right 5, down 1.
// Right 7, down 1.
// Right 1, down 2.
// In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied together, these produce the answer 336.
int solveDay3Part2(RecurringMap& map) {
    int multipliedTreesEncountered = getTreesEncounteredByTraversalMethod(map, 1, 1);
    multipliedTreesEncountered *= getTreesEncounteredByTraversalMethod(map, 3, 1);
    multipliedTreesEncountered *= getTreesEncounteredByTraversalMethod(map, 5, 1);
    multipliedTreesEncountered *= getTreesEncounteredByTraversalMethod(map, 7, 1);
    multipliedTreesEncountered *= getTreesEncounteredByTraversalMethod(map, 1, 2);
    return multipliedTreesEncountered;
}

int main(int argc, char** args) {
    std::cout << "Day 3" << std::endl;

    File exampleFile("../../day3/example.txt");
    File inputFile("../../day3/input.txt");

    // Make a map of trees squares
    RecurringMap exampleMap(exampleFile, '#');
    RecurringMap map(inputFile, '#');

    std::cout << "Result Part 1 example: " << solveDay3Part1(exampleMap) << " (should equal 7)" << std::endl;
    std::cout << "Result Part 1: " << solveDay3Part1(map) << std::endl;

    std::cout << "Partial result Part 2 example (1, 1): " << getTreesEncounteredByTraversalMethod(exampleMap, 1, 1) << " (should equal 2)" << std::endl;
    std::cout << "Partial result Part 2 example (3, 1): " << getTreesEncounteredByTraversalMethod(exampleMap, 3, 1) << " (should equal 7)" << std::endl;
    std::cout << "Partial result Part 2 example (5, 1): " << getTreesEncounteredByTraversalMethod(exampleMap, 5, 1) << " (should equal 3)" << std::endl;
    std::cout << "Partial result Part 2 example (7, 1): " << getTreesEncounteredByTraversalMethod(exampleMap, 7, 1) << " (should equal 4)" << std::endl;
    std::cout << "Partial result Part 2 example (1, 2): " << getTreesEncounteredByTraversalMethod(exampleMap, 1, 2) << " (should equal 2)" << std::endl;

    std::cout << "Result Part 2 example: " << solveDay3Part2(exampleMap) << " (should equal 336)" << std::endl;
    std::cout << "Result Part 2: " << solveDay3Part2(map) << std::endl;
}
