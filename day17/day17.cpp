#include <iostream>
#include <map>

#include "file.h"

/*!
 * the cubes in this region start in the specified active(#) or inactive(.) state.
 *
 * The energy source then proceeds to boot up by executing six cycles.
 *
 * Each cube only ever considers its neighbors : any of the 26 other cubes where any of their coordinates differ by at most 1. 
 * For example, given the cube at x = 1, y = 2, z = 3, its neighbors include the cube at x = 2, y = 2, z = 2, the cube at x = 0, y = 2, z = 3, and so on.
 *
 * During a cycle, all cubes simultaneously change their state according to the following rules :
 *    - If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active.
 *      Otherwise, the cube becomes inactive.
 *    - If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active.
 *      Otherwise, the cube remains inactive.
 */
class PocketDimension {
    int zmin, zmax, ymin, ymax, xmin, xmax;

    std::map<int, std::map<int, std::map<int, bool>>> cubes;

public:
    PocketDimension(File& input) {
        int z = zmin = zmax = 0;

        auto lines = input.getLines();
        int size = lines.size();

        int maximum = ymax = xmax = size / 2;
        int minimum = ymin = xmin = -maximum;

        int y = minimum;
        for (std::string& line : lines) {
            if (line.size() != size) {
                throw std::runtime_error("Input slice is not square");
            }

            int x = minimum;
            for (char c : line) {
                if (c == '#') {
                    cubes[z][y][x] = true;
                }
                else {
                    cubes[z][y][x] = false;
                }

                x++;
            }

            y++;
        }
    }

    int countNeighbouringCubes(int zTarget, int yTarget, int xTarget) {
        int count = 0;
        for (int z = zTarget - 1; z <= zTarget + 1; z++) {
            for (int y = yTarget - 1; y <= yTarget + 1; y++) {
                for (int x = xTarget - 1; x <= xTarget + 1; x++) {
                    if (cubes[z][y][x]) {
                        count++;
                    }
                }
            }
        }

        // Do not count target itself
        if (cubes[zTarget][yTarget][xTarget]) {
            count--;
        }

        return count;
    }

    void growLimits(int zTarget, int yTarget, int xTarget) {
        if (zTarget < zmin) {
            zmin = zTarget;
        }
        else if (zTarget > zmax) {
            zmax = zTarget;
        }

        if (yTarget < ymin) {
            ymin = yTarget;
        }
        else if (yTarget > ymax) {
            ymax = yTarget;
        }

        if (xTarget < xmin) {
            xmin = xTarget;
        }
        else if (xTarget > xmax) {
            xmax = xTarget;
        }
    }

    void cycle() {
        std::map<int, std::map<int, std::map<int, bool>>> newCubes;

        // Check outer rim
        for (int z = zmin - 1; z <= zmax + 1; z++) {
            for (int y = ymin - 1; y <= ymax + 1; y++) {
                for (int x = xmin - 1; x <= xmax + 1; x++) {
                    int neighbours = countNeighbouringCubes(z, y, x);
                    if (cubes[z][y][x]) {
                        // Already active
                        if (neighbours == 2 || neighbours == 3) {
                            // If exactly 2 or 3 neighbours, stay active
                            newCubes[z][y][x] = true;
                            growLimits(z, y, x);
                        }
                        else {
                            // Become inactive
                            newCubes[z][y][x] = false;
                        }
                    }
                    else {
                        // Inactive
                        if (neighbours == 3) {
                            // If exactly 3 neighbours, become active
                            newCubes[z][y][x] = true;
                            growLimits(z, y, x);
                        }
                        else {
                            // Remain inactive
                            newCubes[z][y][x] = false;
                        }
                    }
                }
            }
        }

        // Overwrite state
        cubes = newCubes;
    }

    int countActiveCubes() {
        int count = 0;
        for (int z = zmin; z <= zmax; z++) {
            for (int y = ymin; y <= ymax; y++) {
                for (int x = xmin; x <= xmax; x++) {
                    if (cubes[z][y][x]) {
                        count++;
                    }
                }
            }
        }
        return count;
    }
};

int solveDay17Part1(File& file) {
    PocketDimension dimension(file);

    for (int i = 0; i < 6; i++) {
        dimension.cycle();
    }

    return dimension.countActiveCubes();
}

class PocketDimension4D {
    int wmin, wmax, zmin, zmax, ymin, ymax, xmin, xmax;

    std::map<int, std::map<int, std::map<int, std::map<int, bool>>>> cubes;

public:
    PocketDimension4D(File& input) {
        int w = wmin = wmax = 0;
        int z = zmin = zmax = 0;

        auto lines = input.getLines();
        int size = lines.size();

        int maximum = ymax = xmax = size / 2;
        int minimum = ymin = xmin = -maximum;

        int y = minimum;
        for (std::string& line : lines) {
            if (line.size() != size) {
                throw std::runtime_error("Input slice is not square");
            }

            int x = minimum;
            for (char c : line) {
                if (c == '#') {
                    cubes[w][z][y][x] = true;
                }
                else {
                    cubes[w][z][y][x] = false;
                }

                x++;
            }

            y++;
        }
    }

    int countNeighbouringCubes(int wTarget, int zTarget, int yTarget, int xTarget) {
        int count = 0;
        for (int w = wTarget - 1; w <= wTarget + 1; w++) {
            for (int z = zTarget - 1; z <= zTarget + 1; z++) {
                for (int y = yTarget - 1; y <= yTarget + 1; y++) {
                    for (int x = xTarget - 1; x <= xTarget + 1; x++) {
                        if (cubes[w][z][y][x]) {
                            count++;
                        }
                    }
                }
            }
        }

        // Do not count target itself
        if (cubes[wTarget][zTarget][yTarget][xTarget]) {
            count--;
        }

        return count;
    }

    void growLimits(int wTarget, int zTarget, int yTarget, int xTarget) {
        if (wTarget < wmin) {
            wmin = wTarget;
        }
        else if (wTarget > wmax) {
            wmax = wTarget;
        }

        if (zTarget < zmin) {
            zmin = zTarget;
        }
        else if (zTarget > zmax) {
            zmax = zTarget;
        }

        if (yTarget < ymin) {
            ymin = yTarget;
        }
        else if (yTarget > ymax) {
            ymax = yTarget;
        }

        if (xTarget < xmin) {
            xmin = xTarget;
        }
        else if (xTarget > xmax) {
            xmax = xTarget;
        }
    }

    void cycle() {
        std::map<int, std::map<int, std::map<int, std::map<int, bool>>>> newCubes;

        // Check outer rim
        for (int w = wmin - 1; w <= wmax + 1; w++) {
            for (int z = zmin - 1; z <= zmax + 1; z++) {
                for (int y = ymin - 1; y <= ymax + 1; y++) {
                    for (int x = xmin - 1; x <= xmax + 1; x++) {
                        int neighbours = countNeighbouringCubes(w, z, y, x);
                        if (cubes[w][z][y][x]) {
                            // Already active
                            if (neighbours == 2 || neighbours == 3) {
                                // If exactly 2 or 3 neighbours, stay active
                                newCubes[w][z][y][x] = true;
                                growLimits(w, z, y, x);
                            }
                            else {
                                // Become inactive
                                newCubes[w][z][y][x] = false;
                            }
                        }
                        else {
                            // Inactive
                            if (neighbours == 3) {
                                // If exactly 3 neighbours, become active
                                newCubes[w][z][y][x] = true;
                                growLimits(w, z, y, x);
                            }
                            else {
                                // Remain inactive
                                newCubes[w][z][y][x] = false;
                            }
                        }
                    }
                }
            }
        }

        // Overwrite state
        cubes = newCubes;
    }

    int countActiveCubes() {
        int count = 0;
        for (int w = wmin; w <= wmax; w++) {
            for (int z = zmin; z <= zmax; z++) {
                for (int y = ymin; y <= ymax; y++) {
                    for (int x = xmin; x <= xmax; x++) {
                        if (cubes[w][z][y][x]) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }
};

int solveDay17Part2(File& file) {
    PocketDimension4D dimension(file);

    for (int i = 0; i < 6; i++) {
        dimension.cycle();
    }

    return dimension.countActiveCubes();
}

int main(int argc, char** args) {
    std::cout << "Day 17" << std::endl;

    File exampleFile("../../day17/example.txt");
    File inputFile("../../day17/input.txt");

    std::cout << "Result Part 1 example: " << solveDay17Part1(exampleFile) << " (should equal 112)" << std::endl;
    std::cout << "Result Part 1: " << solveDay17Part1(inputFile) << std::endl;
    std::cout << "Result Part 2 example: " << solveDay17Part2(exampleFile) << " (should equal 848)" << std::endl;
    std::cout << "Result Part 2: " << solveDay17Part2(inputFile) << std::endl;
}
