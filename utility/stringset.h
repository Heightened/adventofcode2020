#pragma once

#include <set>
#include <string>

class StringSet : public std::set<std::string> {
public:
    StringSet();
    StringSet addSet(StringSet& other);
    StringSet subtractSet(StringSet& other);
    StringSet intersectSet(StringSet& other);
};
