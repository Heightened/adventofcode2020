#include "file.h"

#include <string>

File::File(char* fileName) : fileStream(std::ifstream(fileName)) {
	std::cout << "Opening " << fileName << std::endl;

	std::string line;
	while (std::getline(fileStream, line)) {
		lines.push_back(line);
	}
}

std::vector<std::string> File::getLines() {
	return lines;
}
