#include "stringset.h"

StringSet::StringSet() : std::set<std::string>() {
}

StringSet StringSet::addSet(StringSet& other) {
    for (const std::string& setString : other) {
        (*this).insert(setString);
    }
    return *this;
}

StringSet StringSet::subtractSet(StringSet& other) {
    for (const std::string& setString : other) {
        (*this).erase(setString);
    }
    return *this;
}

StringSet StringSet::intersectSet(StringSet& other) {
    StringSet remove;
    for (const std::string& setString : *this) {
        if (other.find(setString) == other.end()) {
            // Not found in the other set, the element should be removed
            remove.insert(setString);
        }
    }
    for (const std::string& setString : remove) {
        (*this).erase(setString);
    }
    return *this;
}
