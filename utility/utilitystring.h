#pragma once

#include <vector>
#include <string>

class String : public std::string {
public:
    String();
    String(std::string string);
    std::vector<String> split(char delimiter = ' ');
    std::vector<String> split(std::string word);
    String strip(char character = ' ');
    size_t count(char character);
    void replaceCharacter(char oldCharacter, char newCharacter);
    void removeDuplicates();
    void removeWord(std::string word);
};
