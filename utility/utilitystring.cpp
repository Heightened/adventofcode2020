#include "utilitystring.h"

#include <vector>
#include <algorithm>

String::String() : std::string() {
}

String::String(std::string string) : std::string(string) {
}

std::vector<String> String::split(char delimiter) {
    std::vector<String> words;
    words.push_back(String());

    int i = 0;
    for (char c : *this) {
        if (c == delimiter) {
            // Start a new word
            if (words[i] != "") {
                i++;
                words.push_back(String());
            }
        }
        else {
            words[i].push_back(c);
        }
    }

    // If the string ends with the delimiter, remove the last empty word from the list
    if (words[i] == "") {
        words.pop_back();
    }

    return words;
}

std::vector<String> String::split(std::string word) {
    std::vector<String> subStrings;

    int offset = 0;
    while (true) {
        // Find the next occurence of the word
        size_t position = (*this).find(word, offset);
        String beforeWord = (*this).substr(offset, position - offset);
        subStrings.push_back(beforeWord);

        if (position == std::string::npos) {
            // Last occurence was found, end of the string will have been pushed to subStrings
            break;
        }

        // new search should start after this word
        offset = position + word.size();
    }

    return subStrings;
}

String String::strip(char character) {
    int offset = 0;
    for (auto it = this->begin(); it != this->end(); it++) {
        if (*it == character) {
            offset++;
        }
        else {
            break;
        }
    }

    int count = this->size() - offset;
    for (auto it = this->rbegin(); it != this->rend(); it++) {
        if (*it == character) {
            count--;
        }
        else {
            break;
        }
    }

    return this->substr(offset, count);
}

size_t String::count(char character) {
    int count = 0;
    for (char c : *this) {
        if (c == character) {
            count++;
        }
    }
    return count;
}

void String::replaceCharacter(char oldCharacter, char newCharacter) {
    std::replace(begin(), end(), oldCharacter, newCharacter);
}

void String::removeDuplicates() {
    String oldString = *this;
    String newString = "";
    for (char c : *this) {
        if (newString.count(c) == 0) {
            newString.push_back(c);
        }
    }
    *this = newString;
}

void String::removeWord(std::string word) {
    auto components = (*this).split(word);
    *this = String();
    for (auto component : components) {
        *this += component;
    }
}
