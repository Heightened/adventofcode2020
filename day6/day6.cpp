#include <iostream>
#include <map>
#include <regex>

#include "file.h"
#include "utilitystring.h"

class CustomsDeclarationForm {
    std::vector<String> responses;

public:
    CustomsDeclarationForm(String formString) {
        responses = formString.split();
    }

    String getUniqueAnswers() {
        String uniqueAnswers = "";
        for (String response : responses) {
            uniqueAnswers += response;
        }
        uniqueAnswers.removeDuplicates();
        return uniqueAnswers;
    }

    size_t getUniqueAnswerCount() {
        return getUniqueAnswers().size();
    }

    String getCommonAnswers() {
        String uniqueAnswers = getUniqueAnswers();
        String commonAnswers = "";

        for (char c : uniqueAnswers) {
            bool sharedAnswer = true;
            // Check if character occurs in each response
            for (String response : responses) {
                // Character occurs in this response
                sharedAnswer &= response.count(c);
            }
            if (sharedAnswer) {
                commonAnswers.push_back(c);
            }
        }

        return commonAnswers;
    }

    size_t getCommonAnswerCount() {
        return getCommonAnswers().size();
    }
};

std::vector<CustomsDeclarationForm> parseForms(File& customsDeclaractionFormFile) {
    std::vector<CustomsDeclarationForm> forms;

    // Parse forms
    auto lines = customsDeclaractionFormFile.getLines();
    // Add empty line at the end to allow last form to be parsed
    lines.push_back("");
    // String to contain multi-line form data
    std::string formString = "";
    // Loop over lines in input data
    for (std::string line : lines) {
        formString += line;
        formString += " "; // Insert seperator

        if (line == "") {
            // Double newline in input file, end of form string
            // Parse the form data
            forms.push_back(CustomsDeclarationForm(formString));

            // Start reading next form
            formString = "";
        }
    }

    return forms;
}


int solveDay5Part1(std::vector<CustomsDeclarationForm>& forms) {
    int sumOfCounts = 0;

    for (auto form : forms) {
        sumOfCounts += form.getUniqueAnswerCount();
    }

    return sumOfCounts;
}

int solveDay5Part2(std::vector<CustomsDeclarationForm>& forms) {
    int sumOfCounts = 0;

    for (auto form : forms) {
        sumOfCounts += form.getCommonAnswerCount();
    }

    return sumOfCounts;
}

int main(int argc, char** args) {
    std::cout << "Day 6" << std::endl;

    File exampleFile("../../day6/example.txt");
    std::vector<CustomsDeclarationForm> exampleForms = parseForms(exampleFile);

    File inputFile("../../day6/input.txt");
    std::vector<CustomsDeclarationForm> inputForms = parseForms(inputFile);

    std::cout << "Result Part 1 example: " << solveDay5Part1(exampleForms) << " (should equal 11)" << std::endl;
    std::cout << "Result Part 1: " << solveDay5Part1(inputForms) << std::endl;
    std::cout << "Result Part 2 example: " << solveDay5Part2(exampleForms) << " (should equal 6)" << std::endl;
    std::cout << "Result Part 2: " << solveDay5Part2(inputForms) << std::endl;
}
