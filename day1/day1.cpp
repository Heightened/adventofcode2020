#include <iostream>

#include "file.h"
#include "utilitystring.h"

int solveDay1Part1(File& inputFile) {
    auto lines = inputFile.getLines();

    // Cross-product of input lines
    for (auto lineA : lines) {
        for (auto lineB : lines) {
            // Interpret lines as integers
            int numberA = std::stoi(lineA);
            int numberB = std::stoi(lineB);

            if (numberA + numberB == 2020) {
                std::cout << "Found numbers " << numberA << ", " << numberB << std::endl;
                return numberA * numberB;
            }
        }
    }
    return -1;
}

int solveDay1Part2(File& inputFile) {
    auto lines = inputFile.getLines();

    // Double Cross-product of input lines
    for (auto lineA : lines) {
        for (auto lineB : lines) {
            for (auto lineC : lines) {
                // Interpret lines as integers
                int numberA = std::stoi(lineA);
                int numberB = std::stoi(lineB);
                int numberC = std::stoi(lineC);

                if (numberA + numberB + numberC == 2020) {
                    std::cout << "Found numbers " << numberA << ", " << numberB << ", " << numberC << std::endl;
                    return numberA * numberB * numberC;
                }
            }
        }
    }
    return -1;
}

int main(int argc, char** args) {
    std::cout << "Day 1" << std::endl;

    File inputFile("../../day1/input.txt");

    std::cout << "Result Part 1: " << solveDay1Part1(inputFile) << std::endl;
    std::cout << "Result Part 2: " << solveDay1Part2(inputFile) << std::endl;
}
