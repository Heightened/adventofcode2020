#include <iostream>
#include <map>
#include <regex>

#include "file.h"
#include "utilitystring.h"

/*!
 * Class representing a Passport
 *
 * Passports can span several lines and can have the following fields
 * byr(Birth Year)
 * iyr(Issue Year)
 * eyr(Expiration Year)
 * hgt(Height)
 * hcl(Hair Color)
 * ecl(Eye Color)
 * pid(Passport ID)
 * cid(Country ID)
 *
 * Only cid is optional
 */
class Passport {
    std::map<std::string, std::string> properties;

public:
    Passport(std::string passportString) {
        // Loop over all space-seperated properties
        auto propertyStrings = ((String)passportString).split();
        for (String property : propertyStrings) {
            // Split key from value
            auto keyValuePair = property.split(':');

            if (keyValuePair.size() != 2) {
                throw std::runtime_error("Incomplete property description");
            }

            // Store property
            properties[keyValuePair[0]] = keyValuePair[1];
        }
    }

    /*!
     * Function to check whether required fields are present
     */
    bool hasRequiredFields() {
        bool requiredPropertiesFound = properties.count("byr");
        requiredPropertiesFound &= properties.count("iyr");
        requiredPropertiesFound &= properties.count("eyr");
        requiredPropertiesFound &= properties.count("hgt");
        requiredPropertiesFound &= properties.count("hcl");
        requiredPropertiesFound &= properties.count("ecl");
        requiredPropertiesFound &= properties.count("pid");
        return requiredPropertiesFound;
    }

    /*!
     * Function to check whether the passport satisfies the following requirements
     * 
     * byr (Birth Year) - four digits; at least 1920 and at most 2002.
     * iyr (Issue Year) - four digits; at least 2010 and at most 2020.
     * eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
     * hgt (Height) - a number followed by either cm or in:
     *     If cm, the number must be at least 150 and at most 193.
     *     If in, the number must be at least 59 and at most 76.
     * hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
     * ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
     * pid (Passport ID) - a nine-digit number, including leading zeroes.
     * cid (Country ID) - ignored, missing or not.
     */
    bool strictlyValid() {
        // Are required fields present
        if (!hasRequiredFields()) {
            return false;
        }

        // Check Birth Year requirements
        int byr = std::stoi(properties["byr"]);
        if (byr < 1920 || byr > 2002) {
            return false;
        }

        // Check Issue Year requirements
        int iyr = std::stoi(properties["iyr"]);
        if (iyr < 2010 || iyr > 2020) {
            return false;
        }

        // Check Expiration Year requirements
        int eyr = std::stoi(properties["eyr"]);
        if (eyr < 2020 || eyr > 2030) {
            return false;
        }

        // Check Height requirements
        std::string heightString = properties["hgt"];
        std::smatch heightMatch;
        if (std::regex_match(heightString, heightMatch, std::regex("^([1-9][0-9]*)cm"))) {
            int heightInCentimeters = std::stoi(heightMatch[1].str());
            if (heightInCentimeters < 150 || heightInCentimeters > 193) {
                return false;
            }
        }
        else if (std::regex_match(heightString, heightMatch, std::regex("^([1-9][0-9]*)in"))) {
            int heightInInches = std::stoi(heightMatch[1].str());
            if (heightInInches < 59 || heightInInches > 76) {
                return false;
            }
        }
        else {
            // Neither height format detected
            return false;
        }

        // Check Hair Color requirements
        std::string hairColorString = properties["hcl"];
        std::smatch hairColorMatch;
        const std::regex hairColorRegex("^#[0-9a-f]{6}$");
        if (!std::regex_match(hairColorString, hairColorMatch, hairColorRegex)) {
            return false;
        }

        // Check Eye Color requirements
        std::string eyeColors[] = { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
        std::string* colorMatch = std::find(std::begin(eyeColors), std::end(eyeColors), properties["ecl"]);
        if (colorMatch == std::end(eyeColors)) {
            // No match
            return false;
        }

        // Check Passport ID requirements
        if (properties["pid"].size() != 9) {
            return false;
        }

        // All requirements are met
        return true;
    }
};

std::vector<Passport> parsePassports(File& passportData) {
    std::vector<Passport> passports;

    // Parse passports
    auto lines = passportData.getLines();
    // Add empty line at the end to allow last passport to be parsed
    lines.push_back("");
    // String to contain multi-line passport data
    std::string passportString = "";
    // Loop over lines in input data
    for (std::string line : lines) {
        passportString += line;
        passportString += " "; // Insert seperator

        if (line == "") {
            // Double newline in input file, end of passport string
            // Parse the passport
            passports.push_back(Passport(passportString));

            // Start reading next passport
            passportString = "";
        }
    }

    return passports;
}

int solveDay4Part1(std::vector<Passport>& passports) {
    int validPassports = 0;

    // Loop over passports
    for (Passport& passport : passports) {
        if (passport.hasRequiredFields()) {
            // Valid passport found
            validPassports++;
        }
    }

    return validPassports;
}

int solveDay4Part2(std::vector<Passport>& passports) {
    int validPassports = 0;

    // Loop over passports
    for (Passport& passport : passports) {
        if (passport.strictlyValid()) {
            // Valid passport found
            validPassports++;
        }
    }

    return validPassports;
}

int main(int argc, char** args) {
    std::cout << "Day 4" << std::endl;

    File exampleFile("../../day4/example.txt");
    auto examplePassports = parsePassports(exampleFile);
    File inputFile("../../day4/input.txt");
    auto inputPassports = parsePassports(inputFile);
    File examplePart2InvalidFile("../../day4/example_part2_invalid.txt");
    auto examplePart2InvalidPassports = parsePassports(examplePart2InvalidFile);
    File examplePart2ValidFile("../../day4/example_part2_valid.txt");
    auto examplePart2ValidPassports = parsePassports(examplePart2ValidFile);

    std::cout << "Result Part 1 example: " << solveDay4Part1(examplePassports) << " (should equal 2)" << std::endl;
    std::cout << "Result Part 1: " << solveDay4Part1(inputPassports) << std::endl;
    std::cout << "Result Part 2 example invalid: " << solveDay4Part2(examplePart2InvalidPassports) << " (should equal 0)" << std::endl;
    std::cout << "Result Part 2 example valid: " << solveDay4Part2(examplePart2ValidPassports) << " (should equal 4)" << std::endl;
    std::cout << "Result Part 2: " << solveDay4Part2(inputPassports) << std::endl;
}
