#include <iostream>

#include "file.h"
#include "utilitystring.h"
#include "stringset.h"

struct RuleComponent {
    int number;
    String descriptor;
};

class Rule {
    String descriptor;
    std::vector<RuleComponent> requiredContents;

public:
    Rule(String ruleString) {
        // Following tokens describe the required contents
        auto ruleSplit = ruleString.split(" contain ");

        // This should have split the string in two
        if (ruleSplit.size() != 2) {
            throw std::runtime_error("Rule is improperly formatted");
        }

        // Get description of the type of bag this rule applies to
        descriptor = ruleSplit[0];
        descriptor.removeWord("bags");
        descriptor = descriptor.strip();

        // Get required contents from second part of the string
        std::vector<String> components = ruleSplit[1].split(',');
        for (String& required : components) {
            auto tokens = required.split();

            if (tokens.size() != 4) {
                // Rule component is improperly formatted
                // No other bags assumed
                return;
            }

            int number = std::stoi(tokens[0]);
            String descriptor = tokens[1] + " " + tokens[2];
            requiredContents.push_back({ number, descriptor });
        }
    }

    std::string getDescriptor() {
        return descriptor;
    }

    int canContain(std::string findDescriptor) {
        for (RuleComponent& contents : requiredContents) {
            if (findDescriptor == contents.descriptor) {
                return contents.number;
            }
        }
        // Not found
        return false;
    }

    std::vector<RuleComponent> getRequiredContents() {
        return requiredContents;
    }
};

class RuleSet {
    std::vector<Rule> rules;

public:
    RuleSet(File& file) {
        auto lines = file.getLines();
        for (auto line : lines) {
            rules.push_back(Rule(line));
        }
    }

    StringSet getDirectBagContainers(std::string descriptor) {
        // Find all bags that can contain a descriptor type bag
        StringSet containers;

        for (Rule& rule : rules) {
            if (rule.canContain(descriptor) > 0) {
                containers.insert(rule.getDescriptor());
            }
        }

        return containers;
    }

    StringSet getRecursiveBagContainers(std::string descriptor, StringSet colored) {
        // Initial containers
        StringSet containers = getDirectBagContainers(descriptor);

        // Which bags should be checked recursively
        StringSet recurse = containers;
        recurse.subtractSet(colored);

        // Ensure that the direct containers are not checked again
        colored.addSet(containers);

        // Look for others recursively
        for (const std::string& recurseDescriptor : recurse) {
            StringSet recursiveContainers = getRecursiveBagContainers(recurseDescriptor, colored);
            containers.addSet(recursiveContainers);
        }

        return containers;
    }

    std::set<std::string> getRecursiveBagContainers(std::string descriptor) {
        return getRecursiveBagContainers(descriptor, {});
    }

    int getContainedBags(std::string descriptors) {
        int total = 0;

        // Find rule
        for (Rule& rule : rules) {
            if (descriptors == rule.getDescriptor()) {
                // Recursive addition
                for (RuleComponent component : rule.getRequiredContents()) {
                    total += component.number;
                    total += component.number * getContainedBags(component.descriptor);
                }

                // Done (assuming there is only one rule per bag type)
                break;
            }
        }

        return total;
    }
};

int solveDay7Part1(RuleSet& rules) {
    return rules.getRecursiveBagContainers("shiny gold").size();
}

int solveDay7Part2(RuleSet& rules) {
    return rules.getContainedBags("shiny gold");
}

int main(int argc, char** args) {
    std::cout << "Day 7" << std::endl;

    File exampleFile("../../day7/example.txt");
    RuleSet exampleRules(exampleFile);

    File inputFile("../../day7/input.txt");
    RuleSet inputRules(inputFile);

    File exampleFilePart2("../../day7/example_part2.txt");
    RuleSet exampleRulesPart2(exampleFilePart2);

    std::cout << "Result Part 1 example: " << solveDay7Part1(exampleRules) << " (should equal 4)" << std::endl;
    std::cout << "Result Part 1: " << solveDay7Part1(inputRules) << std::endl;
    std::cout << "Result Part 2 example 1: " << solveDay7Part2(exampleRules) << " (should equal 32)" << std::endl;
    std::cout << "Result Part 2 example 2: " << solveDay7Part2(exampleRulesPart2) << " (should equal 126)" << std::endl;
    std::cout << "Result Part 2: " << solveDay7Part2(inputRules) << std::endl;
}
