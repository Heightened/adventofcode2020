#include <iostream>
#include <map>

#include "file.h"
#include "utilitystring.h"

class DecoderEmulator {
protected:
    std::map<uint64_t, uint64_t> memory;

public:
    virtual void updateBitMask(String bitmask) = 0;
    virtual void writeValue(uint64_t index, uint64_t value) = 0;

    uint64_t getSum() {
        uint64_t sum = 0;
        for (auto value : memory) {
            sum += value.second;
        }
        return sum;
    }
};

class StandardDecoderEmulator : public DecoderEmulator {
    uint64_t bitMaskOnes;
    uint64_t bitMaskZeroes;

public:
    void updateBitMask(String bitmask) {
        if (bitmask.size() != 36) {
            throw std::runtime_error("Wrong bitmask size");
        }

        // Make a string with all zeroes except for the 1's in the mask
        String ones = bitmask;
        ones.replaceCharacter('X', '0');
        bitMaskOnes = stoll (ones, nullptr, 2);

        // Make a string with all ones except for the 0's in the mask
        String zeroes = bitmask;
        zeroes.replaceCharacter('X', '1');
        bitMaskZeroes = stoll(zeroes, nullptr, 2);
    }

    virtual void writeValue(uint64_t index, uint64_t value) {
        // Bitwise or will force the one positions in bitMaskOnes
        value |= bitMaskOnes;

        // Bitwise and will force the zero positions in bitMaskZeroes
        value &= bitMaskZeroes;

        memory[index] = value;
    }
};

class FloatingDecoderEmulator : public DecoderEmulator {
    std::vector<std::pair<uint64_t, uint64_t>> floatingBitMasks;

    std::vector<std::string> getFloatingMasks(std::string bitmask) {
        std::vector<std::string> floatingMaskStrings;

        // Insert empty string to start
        floatingMaskStrings.push_back("");

        // Loop over input bitmask
        for (char c : bitmask) {
            if (c == 'X') {
                std::vector<std::string> tempCopy(floatingMaskStrings.begin(), floatingMaskStrings.end());
                // Extend all existing floatingMaskStrings with 0
                for (std::string& mask : floatingMaskStrings) {
                    mask += '0';
                }
                // Add one for each pre-existing floatingMaskString with a 1 appended
                for (std::string& mask : tempCopy) {
                    floatingMaskStrings.push_back(mask + '1');
                }
            }
            else if (c == '0') {
                // Should not affect the input, like 'X' in part 1
                for (std::string& mask : floatingMaskStrings) {
                    mask += 'X';
                }
            }
            else if (c == '1') {
                // 1 should be forced, like before
                for (std::string& mask : floatingMaskStrings) {
                    mask += '1';
                }
            }
            else {
                throw std::runtime_error("Syntax error, mask contains unknown character");
            }
        }
        return floatingMaskStrings;
    }

public:
    virtual void updateBitMask(String bitmask) override {
        // Construct all floating masks
        floatingBitMasks.clear();
        auto floatingMasks = getFloatingMasks(bitmask);
        for (String floatingBitMask : floatingMasks) {
            // Make a string with all zeroes except for the 1's in the mask
            String floatingOnes = floatingBitMask;
            floatingOnes.replaceCharacter('X', '0');

            // Make a string with all ones except for the 0's in the mask
            String floatingZeroes = floatingBitMask;
            floatingZeroes.replaceCharacter('X', '1');

            // Push back floating mask numbers
            floatingBitMasks.push_back({ stoll(floatingOnes, nullptr, 2), stoll(floatingZeroes, nullptr, 2) });
        }
    }

    virtual void writeValue(uint64_t index, uint64_t value) override {
        for (auto bitMask : floatingBitMasks) {
            // Bitwise or will force the one positions in floating bitMask ones
            index |= bitMask.first;

            // Bitwise and will force the zero positions in floating bitMask zeroes
            index &= bitMask.second;

            memory[index] = value;
        }
    }
};

void interpretInitializationProgram(DecoderEmulator* emulator, File& file) {
    auto lines = file.getLines();
    for (String line : lines) {
        auto tokens = line.split();

        // Sanity check
        if (tokens.size() != 3) {
            throw std::runtime_error("Syntax error, wrong number of tokens");
        }

        // Parse, line by line
        if (tokens[0] == "mask") {
            emulator->updateBitMask(tokens[2]);
        }
        else {
            // Assumed mem command
            auto memTokens = tokens[0].split('[');

            // Sanity check 2
            if (memTokens[0] != "mem") {
                throw std::runtime_error("Syntax error, unknown keyword");
            }

            // Write value
            int index = stoi(memTokens[1].strip(']'));
            emulator->writeValue(index, stoi(tokens[2]));
        }
    }
}

uint64_t solveDay14Part1(File& file) {
    DecoderEmulator* emulator = new StandardDecoderEmulator();

    interpretInitializationProgram(emulator, file);

    return emulator->getSum();
}

uint64_t solveDay14Part2(File& file) {
    DecoderEmulator* emulator = new FloatingDecoderEmulator();

    interpretInitializationProgram(emulator, file);

    return emulator->getSum();
}

int main(int argc, char** args) {
    std::cout << "Day 14" << std::endl;

    File exampleFile("../../day14/example.txt");
    File inputFile("../../day14/input.txt");
    File examplePart2File("../../day14/example_part2.txt");

    std::cout << "Result Part 1 example: " << solveDay14Part1(exampleFile) << " (should equal 165)" << std::endl;
    std::cout << "Result Part 1: " << solveDay14Part1(inputFile) << std::endl;
    std::cout << "Result Part 2 example: " << solveDay14Part2(examplePart2File) << " (should equal 208)" << std::endl;
    std::cout << "Result Part 2: " << solveDay14Part2(inputFile) << std::endl;
}
